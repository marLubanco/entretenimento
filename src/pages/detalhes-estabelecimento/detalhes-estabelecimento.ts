import { CumpomModalPage } from './../cumpom-modal/cumpom-modal';
import { Estabelecimento } from './../../model/estabelecimento.model';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, ModalController } from 'ionic-angular';

@Component({
  selector: 'page-detalhes-estabelecimento',
  templateUrl: 'detalhes-estabelecimento.html',
})
export class DetalhesEstabelecimentoPage {
  @ViewChild(Slides) slides: Slides;
  estabelecimento: Estabelecimento = new Estabelecimento;

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    this.estabelecimento = navParams.get('todo')
    console.log(this.estabelecimento.cnpj,this.estabelecimento.nome)
  }

  presentContactModal() {
    var data = { message : 'hello world' };
    var modalPage = this.modalCtrl.create(CumpomModalPage);
    modalPage.present();
  }

  goToSlide() {
    this.slides.slideTo(2, 500);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }

}
