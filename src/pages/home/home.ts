import { Observable } from 'rxjs';
import { DetalhesEstabelecimentoPage } from './../detalhes-estabelecimento/detalhes-estabelecimento';
import { LoginPage } from './../login/login';
import { AuthService } from './../../providers/auth/auth.service';
import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { EstabelecimentoService } from '../../providers/estabelecimento/estabelecimento.service';


@Component({
  templateUrl: 'home.html'
})

export class HomePage {
  public todos: any[] = [];
  canSearch: boolean = false;

  constructor(public navCtrl: NavController,  public menu: MenuController,
    public authService: AuthService, private _estabelecimentoService: EstabelecimentoService) {
     menu.enable(true);
    this.initializeItems();
  }



  initializeItems() {
    let that = this;
    this._estabelecimentoService.todos.subscribe((data) => {
      that.todos.push(data);
    },
      (err) => {
        console.error(err);
      });
  };

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.todos = this.todos.filter((todo) => {
        return (todo.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  itemTapped(event, todo) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(DetalhesEstabelecimentoPage, {
      todo: todo
    });
  }

}