import { Component } from '@angular/core';
import {  NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-cumpom-modal',
  templateUrl: 'cumpom-modal.html',
})
export class CumpomModalPage {
  constructor(public navCtrl: NavController, public viewCtrl : ViewController ,public navParams: NavParams) {
  }
  public closeModal(){
      this.viewCtrl.dismiss();
  }
  ionViewDidLoad() {
      console.log('ionViewDidLoad ModalPage');
  }
}
