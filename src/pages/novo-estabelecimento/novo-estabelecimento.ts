
import { TelefoneValidator } from './../../validators/telefone.validator';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams, ToastController, Loading, LoadingController, AlertController, Alert } from 'ionic-angular';
import { Estabelecimento } from '../../model/estabelecimento.model';
import { EstabelecimentoService } from '../../providers/estabelecimento/estabelecimento.service';
import { HomePage } from '../home/home';
import { EmailValidator } from '../../validators/email.validator';


@Component({
  selector: 'page-novo-estabelecimento',
  templateUrl: 'novo-estabelecimento.html',
})
export class NovoEstabelecimentoPage {

  signInType:string = 'cadastro';


  public newEstabelecimentoForm: FormGroup;
  public estabelecimento: Estabelecimento;
  private filePhoto: File[];
  uploadProgress: number;
  myDate: String = new Date().toISOString();


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public estabelecimentoService: EstabelecimentoService,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    formBuilder: FormBuilder,
) {
    this.newEstabelecimentoForm = formBuilder.group({
      email: [
        '',
        Validators.compose([Validators.required, EmailValidator.isValid])
      ],
      nome: [
        '',
        Validators.compose([Validators.required])
      ],
      cnpj: [
        '',
        Validators.required
      ],
      telefone: [
        '',
        Validators.compose([Validators.required])
      ],
      whatsapp: [
        '',
        Validators.compose([Validators.required])
      ],
      endereco: [
        '',
        Validators.required
      ],
      descricao: [
        '',
        Validators.required
      ]

    });
  }

  onPhoto(event): void {
    this.filePhoto = event.target.files[event];
  }
  
  proximo(){
    this.signInType ='fotos';
  }
  async save(): Promise<void> {

    if (!this.newEstabelecimentoForm.valid) {
      console.log(
        `Form is not valid yet, current value: ${this.newEstabelecimentoForm.value}`
      );
    } else {
      const loading: Loading = this.loadingCtrl.create();
      loading.present();
      this.estabelecimento = new Estabelecimento;
      this.estabelecimento.nome = this.newEstabelecimentoForm.value.nome;
      this.estabelecimento.email = this.newEstabelecimentoForm.value.email;
      this.estabelecimento.cnpj = this.newEstabelecimentoForm.value.cnpj;
      this.estabelecimento.telefone = this.newEstabelecimentoForm.value.telefone;
      this.estabelecimento.whatsapp = this.newEstabelecimentoForm.value.whatsapp;
      this.estabelecimento.endereco = this.newEstabelecimentoForm.value.endereco;
      //this.estabelecimento.funcHorario = this.newEstabelecimentoForm.value.funcHorario;
      this.estabelecimento.descricao = this.newEstabelecimentoForm.value.descricao;
      this.estabelecimento.data = this.myDate;
      this.estabelecimento.done = false;
      try {
        var key = this.estabelecimentoService.save(this.estabelecimento);
        if (key) {
          loading.dismiss();
          this.presentToast('Cadastrado com sucesso');
        }
      } catch (error) {
        console.log(error);
        loading.dismiss();
        this.showAlert(error);
        this.navCtrl.getPrevious();
      };
    }
  }


  private showAlert(message: string): void {
    this.alertCtrl.create({
      message: message,
      buttons: ['Ok']
    }).present();
  }

  presentToast(msg: String) {
    let toast = this.toastCtrl.create({
      message: '' + msg,
      duration: 3000,
      position: 'down'
    }).present();
    this.navCtrl.setRoot(HomePage).then((hasAccss: boolean) => {
      console.log('Autorizado', hasAccss)
    }).catch(err => {
      this.presentToast(err)
    });
  }

  @ViewChild('descricao') descricao: ElementRef;
  resize() {
    var element = this.descricao['_elementRef'].nativeElement.getElementsByClassName("text-input")[0];
    var scrollHeight = element.scrollHeight;
    element.style.height = scrollHeight + 'px';
    this.descricao['_elementRef'].nativeElement.style.height = (scrollHeight + 16) + 'px';
  }
}
