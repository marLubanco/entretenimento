import { AngularFireList } from 'angularfire2/database';

import { Estabelecimento } from './../../model/estabelecimento.model';


import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Injectable, Inject } from '@angular/core';
import { FirebaseApp } from 'angularfire2';
import { BaseService } from '../base/base.service';
import firebase from 'firebase'

@Injectable()
export class EstabelecimentoService {
  estabelecimentos: AngularFireList<Estabelecimento[]>;
  private _todos$: any;
  private _db: any;
  private _todosRef: any;
  private _openRef: any;
  private _opens$: any;


  constructor(@Inject(FirebaseApp) public firebaseApp: any, ) {
    this._db = firebase.database().ref('/');
    this._todosRef = firebase.database().ref('estabelecimentos');
    this._todosRef.on('child_added', this.handleData, this);
    this._todos$ = new ReplaySubject();
  }
  get todos() {
    return this._todos$;
  }


  save(estabelecimento: Estabelecimento) {
    return this._todosRef.push(estabelecimento).key;
  }

  handleData(snap) {
    //Do something with the data 
    try {
      this._todos$.next(snap.val());
    } catch (error) {
      console.log('catching', error);
    }
  }

  get allOpened() {
    // Loop through users in order with the forEach() method. The callback
    // provided to forEach() will be called synchronously with a DataSnapshot
    // for each child:
    var query = firebase.database().ref("estabelecimentos").orderByKey();
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          // key will be "ada" the first time and "alan" the second time
          var key = childSnapshot.key;
          // childData will be the actual contents of the child
          var childData = childSnapshot.val();
          return childData;
        });
      });
    return query;

  }

  uploadPhoto(file: File, estabelecimentoId: string): firebase.storage.UploadTask {
    return this.firebaseApp
      .storage()
      .ref()
      .child(`/estabelecimentos/${estabelecimentoId}`)
      .put(file);
  }

  public removeFile(fullPath: string) {
    let storageRef = this.firebaseApp.storage().ref();
    storageRef.child(fullPath).delete();
  }
}
