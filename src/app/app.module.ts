import { PreloaderProvider } from './../providers/preloader/preloader.service';
import { CustomLoggedHeaderComponent } from './../components/custom-logged-header/custom-logged-header.component';
import { UserMenuComponent } from './../components/user-menu/user-menu.component';


import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserInfoComponent } from './../components/user-info/user-info.component';
import { AuthService } from './../providers/auth/auth.service';

import { ResetPasswordPage } from './../pages/reset-password/reset-password';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EstabelecimentoService } from '../providers/estabelecimento/estabelecimento.service';
import { NovoEstabelecimentoPage } from '../pages/novo-estabelecimento/novo-estabelecimento';

import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { DetalhesEstabelecimentoPage } from '../pages/detalhes-estabelecimento/detalhes-estabelecimento';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { UserService } from '../providers/user/user.service';
import { CapitalizePipe } from '../pipes/capitalize/capitalize';
import { ImageService } from '../providers/image/image.service';
import { UserProfilePage } from './../pages/user-profile/user-profile';
import { ProgressBarComponent } from './../components/progress-bar/progress-bar.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { CumpomModalPage } from '../pages/cumpom-modal/cumpom-modal';
import { HttpModule } from '@angular/http';


export var firebaseConfig = {
  apiKey: "AIzaSyAmzdSvpJN2R7vPCpxvChjThpinqWQr0C0",
  authDomain: "entretenimento-5ecc8.firebaseapp.com",
  databaseURL: "https://entretenimento-5ecc8.firebaseio.com",
  projectId: "entretenimento-5ecc8",
  storageBucket: "gs://entretenimento-5ecc8.appspot.com",
  messagingSenderId: "479468673685"
};

@NgModule({
  declarations: [
    CapitalizePipe,
    CustomLoggedHeaderComponent,
    CumpomModalPage,
    DetalhesEstabelecimentoPage,
    HomePage,
    LoginPage,
    MyApp,
    NovoEstabelecimentoPage,
    ProgressBarComponent,
    ResetPasswordPage,
    SignupPage,
    UserInfoComponent,
    UserMenuComponent,
    UserProfilePage
  ],
  imports: [
    BrMaskerModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule,
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CumpomModalPage,
    DetalhesEstabelecimentoPage,
    HomePage,
    LoginPage,
    MyApp,
    NovoEstabelecimentoPage,
    ResetPasswordPage,
    SignupPage,
    UserProfilePage
  ],
  providers: [
    AuthService,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    EstabelecimentoService,
    UserService,
    ImageService,
    PreloaderProvider
  ]
})
export class AppModule { }
