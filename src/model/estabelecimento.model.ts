import { Semana } from "./semana.model";

export class Estabelecimento {
    nome: String;
    cnpj: String;
    descricao: String;
    telefone: String;
    whatsapp: String;
    email: String;
    endereco: String;
    funcHorario: Semana;
    photo: string[];
    done: boolean;
    data: String

}